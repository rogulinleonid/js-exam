class Tab {
  constructor(tabsArr) {
    this.tabsArr = tabsArr
    this.activeTab = 0
  }

  render() {
    let elWrapper = document.createElement('div')
    elWrapper.className = 'tab__items'
    let counter = 0
    for (const tab of this.tabsArr) {
      let el = document.createElement('div')
      el.className = 'tab__item'
      el.id = counter++
      el.innerText = tab.title
      elWrapper.appendChild(el)
    }
    return elWrapper
  }

  getData() {
    let el = document.createElement('div')
    console.log(this.activeTab)
    el.innerText = this.tabsArr[this.activeTab].data
    el.className = 'dataContent'
    return el
  }
}

// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`
  }
])

// Вставляем Таб на страницу и вызываем метод рэендер
document.getElementById('tab').appendChild(tab.render())
document.getElementById('tab').append(tab.getData())

let tabItems = document.getElementsByClassName('tab__item')
for (let i = 0; i < tabItems.length; i++) {
  tabItems[i].addEventListener('click', () => {
    tab.activeTab = i
    let a = document.querySelector('.tab__items')
    a.remove()
    let b = document.querySelector('.dataContent')
    b.remove()
    document.getElementById('tab').appendChild(tab.render())
    document.getElementById('tab').append(tab.getData())
  })
}